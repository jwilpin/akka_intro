import akka.actor.{ActorLogging, Props}
import akka.{actor => classic}

object ClassicPingPong extends App {

  class ClassicPing extends classic.Actor with ActorLogging {

    log.info(s"starting pong actor...")
    val pongActorRef = context.actorOf(Props[ClassicPong], "pongActor")
    log.info(s"$pongActorRef actor started!")

    pongActorRef ! "ping"

    override def receive: Receive = {
      case "pong" =>
        log.info(s"$self got Pong from ${sender()}")
        context.stop(self)
      case _ =>
        log.info("received unknown message")
        context.stop(self)
    }
  }

  class ClassicPong extends classic.Actor with ActorLogging {
    override def receive: Receive = {
      case "ping" =>
        log.info(s"$self got Ping from ${sender()}")
        sender() ! "pong"
        context.stop(self)
      case _ =>
        log.info("received unknown message")
        context.stop(self)
    }
  }

  val system = classic.ActorSystem("ClassicPingPong")
  val pingActorRef = system.actorOf(Props[ClassicPing], "pingActor")
}
