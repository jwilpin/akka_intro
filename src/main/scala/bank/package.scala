package object bank {

  case class Account(number: Long, amount: Long){
    def add(credit: Long): Account = Account(number, amount + credit)
    def draft(debit: Long): Account = Account(number, amount - debit)
  }
}
