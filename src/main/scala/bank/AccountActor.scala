package bank

import akka.actor.typed._
import akka.actor.typed.scaladsl._

object AccountActor {

  sealed trait AccountCommand
  case class Draft(debit: Long, replyTo: ActorRef[AccountCommandResult]) extends AccountCommand
  case class Add(credit: Long, replyTo: ActorRef[AccountCommandResult]) extends AccountCommand

  sealed trait AccountCommandResult
  case class Accepted(newAmount: Long) extends AccountCommandResult
  case class Rejected(cause: String) extends AccountCommandResult

  def behavior(account: Account): Behavior[AccountCommand] = {
    Behaviors.receiveMessage[AccountCommand]{
      case Add(credit, replyTo) =>
        val updated = account.add(credit)
        replyTo ! Accepted(updated.amount)
        behavior(updated)
      case Draft(debit, replyTo) if account.amount - debit >= 0 =>
        val updated = account.draft(debit)
        replyTo ! Accepted(updated.amount)
        behavior(updated)
      case Draft(_, replyTo) =>
        replyTo ! Rejected("invalid amount")
        Behaviors.same
    }
  }
}
