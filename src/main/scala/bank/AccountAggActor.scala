package bank

import akka.actor.typed._
import akka.actor.typed.scaladsl._

object AccountAggActor {

  case class Transaction(accountNumber: Long, amount: Long, replyTo: ActorRef[TransactionResult])

  sealed trait TransactionResult
  case class Accepted(accountNumber: Long, amount: Long) extends TransactionResult
  case class Rejected(accountNumber: Long, amount: Long, cause: String) extends TransactionResult

  def behavior(state: Map[Long, ActorRef[AccountActor.AccountCommand]] = Map.empty): Behavior[Transaction] = {
//    Behaviors.receiveMessage{
//      case Transaction(accountNumber, amount, replyTo) =>
//        state.get(accountNumber).map{
//          accountActor =>
//
//            Behaviors.same
//        }.getOrElse{
//          replyTo ! Rejected(s"account $accountNumber does not exists")
//          Behaviors.same
//        }
//    }
    ???
  }
}
