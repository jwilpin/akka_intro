import scala.concurrent.{Await, Future, blocking}
import scala.concurrent.ExecutionContext.global
import scala.concurrent.duration._

object FutureApp extends App {

  implicit val ec = global

//  for( i <- 1 to 32000 ) {
//    Future {
//      blocking {
//        Thread.sleep(1000)
//        println(s"thread $i is awake")
//      }
//    }
//  }
//
//  Thread.sleep(1000000)


  val fut1: Future[Int] = Future{
    println("Future 1")
    Thread.sleep(1000)
    1
  }

  val fut2:Future[Int] = Future{
    println("Future 2")
    Thread.sleep(3000)
    2
  }

  val fut3: Future[Int] = for {
    fut1Val <- fut1
    fut2Val <- fut2
  } yield fut1Val + fut2Val

  println(Await.ready(fut3, 5.seconds))

}

