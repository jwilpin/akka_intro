import akka.actor.typed._
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.scaladsl._
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

object PingPong extends App {

  sealed trait Command
  final case class Ping(sender: ActorRef[Pong]) extends Command
  case class Pong()

  def pingBehavior: Behavior[Ping] = {
    Behaviors.receiveMessage{
      case Ping(sender) =>
        //ctx.log.info(s"${ctx.self} got Ping from $sender")
        sender ! Pong()
        Behaviors.stopped
    }
  }

  def pongBehavior: Behavior[Pong] = {
    Behaviors.receive{
      case (ctx, _) =>
        ctx.log.info(s"${ctx.self} got Pong from ???")
        Behaviors.stopped
    }
  }

  implicit val system: ActorSystem[SpawnProtocol.Command] = {
    ActorSystem(Behaviors.setup[SpawnProtocol.Command] { _ =>
      SpawnProtocol()
    }, "PingPong")
  }

  implicit val ec: ExecutionContext = system.executionContext
  implicit val timeout: Timeout = Timeout(3.seconds)

  val pingActorRefFut: Future[ActorRef[Ping]] =
    system.ask(SpawnProtocol.Spawn(pingBehavior, name = "ping", props = Props.empty, _))

  val pongActorRefFut: Future[ActorRef[Pong]] =
    system.ask(SpawnProtocol.Spawn(pongBehavior, name = "pong", props = Props.empty, _))

  for{
    pongActorRef <- pongActorRefFut
    pingActorRef <- pingActorRefFut
  } yield {
    pingActorRef ! Ping(pongActorRef)
  }
}
